var nombreDeCaracteres = 256;
class Rabin_Karp_Visual{
    constructor(texte,chaine,nombrePremier,obj){
        this.texte = texte;
        this.chaine = chaine;
        this.nombrePremier=nombrePremier;
        
        this.hashChaine=0;
        this.hashTexte=0;
        this.hashValeur=1;
        
        this.index=0;
        this.indice;
        
        this.obj = obj;
    }
    recherche(){
        
            //Calcul la valeur de hachage de la prochaine portion du texte en utilisant la propriété de l'empreinte de Rabin : retirer le bit de poids fort du hash courant et ajouter le bit de poids faible du hash suivant
            if(this.index < this.texte.length - this.chaine.length){
               $("#stext").empty();
               $("#stext").append($("<h3> Empreinte de Rabin : </h3><span>Ancienne valeur de la portion de texte : "+this.hashTexte+"</span><br>"))
                       .append($("<span> Code ASCII de "+this.texte.charAt(this.index+1)+" : "+this.texte.charCodeAt(this.index+1)+"</span><br>"))
                       .append($("<span> Code ASCII de "+this.texte.charAt(this.index+2)+" : "+this.texte.charCodeAt(this.index+2)+"</span><br>"))
                       .append($("<span> Nombre de caractères ASCII : 256</span><br>"))
                       .append($("<span> Nombre premier : 101</span><br>"));
               $("#stext").append($("<span> ( 256 * ( "+this.hashTexte+" - "+this.texte.charCodeAt(this.index+1)+" * "+this.hashValeur+" ) + "+this.texte.charCodeAt(this.index+2)+" ) % 101 = </span>"));
                this.hashTexte = (nombreDeCaracteres*(this.hashTexte - this.texte.charCodeAt(this.index)*this.hashValeur)+this.texte.charCodeAt(this.index+this.chaine.length))%this.nombrePremier;
                //au cas où le hash du texte est négative
                if(this.hashTexte < 0)
                    this.hashTexte = (this.hashTexte + this.nombrePremier);
             this.index++;
             $("#stext").append($("<span><b>"+this.hashTexte+"</b></span>"));
            }
            
        
        
    }
    init(){
        for(this.index=0;this.index<this.chaine.length-1;this.index++){
            this.hashValeur = (this.hashValeur*nombreDeCaracteres)%this.nombrePremier;
        }
        //Calcul la valeur de hachage de la chaine et de la premiere portion du texte
        for(this.index = 0; this.index < this.chaine.length;this.index++){
            $("#stext").append($("<span>Valeur de hachage de "+this.texte.charAt(this.index)+" : ( 256 * "+this.hashTexte+" + "+this.texte.charCodeAt(this.index)+" ) % 101 = </span>"));
            this.hashChaine = (nombreDeCaracteres*this.hashChaine + this.chaine.charCodeAt(this.index))%this.nombrePremier;
            this.hashTexte = (nombreDeCaracteres*this.hashTexte + this.texte.charCodeAt(this.index))%this.nombrePremier;
            $("#stext").append($("<span>"+this.hashTexte+"</span><br>"));
        }
        $("#stext").append($("<br><span>Valeur de hachage de la portion de texte : "+this.hashTexte+"</span>"));
        this.index=0;
        return [this.hashTexte,this.hashChaine];
    }

}
var rkv;
function start(){
    var textarea = $("#text");
    var input = $("input");
    var bout = $("#change");
    if(textarea.val()!="" && input.val()!=""){
        $('#param').hide(400);
        $("#res").show(800,function(){
            $("#txt").text(textarea.val());
            $("#motif").text(input.val());
            rkv = new Rabin_Karp_Visual(textarea.val(),input.val(),101,bout);
            var arr = rkv.init();
            long_assist(rkv.texte,input.val(),rkv.index,arr[0],arr[1]);
            changeButton("next");
        });
    }
    else{
        alert("Texte et/ou motif vide!");
    }
}
function next(){
    if(rkv.index == rkv.texte.length - rkv.chaine.length)
        changeButton("suiv");
    rkv.recherche();
    long_assist(rkv.texte,$("#motif").text(),rkv.index,rkv.hashTexte,rkv.hashChaine);
}
function changeButton(chaine){
    if(chaine==="next"){
        $("#change").attr({'onclick':'next()','id':'next'}).text("Suivant");
        $("#prec").attr({'id':'prec2'}).text("Reset");
    }
    if(chaine==="suiv"){
        $("#next").attr({'onclick':'otherSwitch(this)','id':'suiv2'}).text("Page Suivante");
    }
    if(chaine==="change"){
        $("#suiv").attr({'onclick':'start()','id':'change'}).text("Analyse pas à pas");
        $("#prec").show();
    }
    else{
        
    }
}
function otherSwitch(obj){
    if($(obj).attr('id')==='prec'){
        document.location.href="explain.html";
    }
    if($(obj).attr('id')==='prec2'){
        document.location.href="resume.html";
    }
    if($(obj).attr('id')==='suiv2'){
        document.location.href="multipleMotifs.html";
    }
}
function long_assist(texte,motif,start,hashTexte,hashChaine){
    var txt ="";
    for(var j=start;j<motif.length + start;j++){
        txt = txt + texte.charAt(j);
    }
    $("#txt").text("");
    var once = true;
    for(var i=0;i<texte.length;){
        if(i==start){
            for(var j=0;j<motif.length;j++){
                if(hashTexte==hashChaine){
                    if(txt==motif){
                        if(once){
                            $("#match").append($("<p>Chaine trouvé à l'indice "+(i+1)+"</p>"));
                            once=false;
                        }
                        $("#txt").append($("<b>"+texte.charAt(i+j)+"</b>").css({"color":"#539731"}));
                    }
                    else
                        $("#txt").append($("<b>"+texte.charAt(i+j)+"</b>").css({"color":"#ce0c0c"}));
                    }
                else
                    $("#txt").append($("<b>"+texte.charAt(i+j)+"</b>").css({"color":"#ce0c0c"}));
                        
            }
            i = i + motif.length;
        }
        else{
            $("#txt").append($("<span>"+texte.charAt(i)+"</span>"));
            i++;
        }
    }
    $("#smotif").empty();
    $("#smotif").append($("<span>Hash de "+motif+" : <b>"+hashChaine+"</b></span>"));
}
