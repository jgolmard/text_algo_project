function createCookie(cname,cvalue,days){
    var d = new Date();
    d.setTime(d.getTime()+(days*24*60*60*1000));
    var exp = "expires="+d.toUTCString();
    document.cookie = cname +"="+cvalue+";"+exp+";path=/";
}
function getCookie(cname){
    var name = cname +"=";
    var ca = document.cookie.split(";");
    for(var i=0;i<ca.length;i++){
        var c = ca[i];
        while(c.charAt(0)==' '){
            c=c.substring(1);
        }
        if(c.indexOf(name)==0){
            return c.substring(name.length,c.length);
        }
    }
    if(cname=="text")
        return "AAABCABACCABBCAC";
    if(cname=="motif")
        return "CA";
}
function changePage(){
    var text = $("#text").val();
    var motif = $("input").val();
    if(text!="" && motif!=""){
        createCookie("text",text,1);
        createCookie("motif",motif,1);
        document.location.href="Ressources/explain.html";
    }
    else{
        alert("Texte et/ou motif vide!");
    }
    
}
function initiate(){
    var text = getCookie("text");
    var motif = getCookie("motif");
    $("#text").text(text);
    $("#motif").text("Motif : "+motif);
    assist_research(text,motif,0,256,101);
    $(".sit1 > p").css({"padding-top":"15px"});
}
var expl = true;
function assist_research(texte,motif,start,nbCarac,nbPremier){
    var ht=0;
    var hm=0;
    var txt="";
    var tx="";
    for(var i=start;i<texte.length;i++){
        tx = tx + texte[i];
    }
    for(var i=start;i<motif.length+start;i++){
        txt= txt + texte.charAt(i);
        ht = (nbCarac*ht + texte.charCodeAt(i))%nbPremier;
    }
    for(var i=0;i<motif.length;i++){
       hm = (nbCarac*hm + motif.charCodeAt(i))%nbPremier;  
    }
    var arr = tx.split(txt);
    var fin = [arr.shift(), arr.join(txt)];

    $("#text").text("");
    
    $("#stext").text("Hash de "+txt+" : "+ht);
    $("#smotif").text("Hash de "+motif+" : "+hm);
    if(hm!=ht){
        $("#res").text("Les valeurs de hash sont différentes, donc on passe à la portion de texte suivante.");
        $("#text").append(texte.split(tx)[0])
                  .append(fin[0])
                  .append($("<b>"+txt+"</b>").css({"color":"#ce0c0c"}))
                  .append(fin[1]);
        return false;
    }
    else{
        if(motif===txt){
          $("#res").text("Les valeurs de hash sont égales, on compare désormais caractère par caractère. \n\
On observe donc que le motif et la portion de texte sont égaux, nous avons donc un match.");
          $("#text").append(texte.split(tx)[0])
                    .append(fin[0])
                    .append($("<b>"+txt+"</b>").css({"color":"#539731"}))
                    .append(fin[1]);
            return true;
        }
        else{
            $("#res").text("Les valeurs de hash sont égales, on compare désormais caractère par caractère. Ici on remarque que\n\
le motif et la portion de texte sont différents malgré une valeur de hachage similaire, on passe à la portion de texte suivante.");
            $("#text").append(texte.split(tx)[0])
                      .append(fin[0])
                      .append($("<b>"+txt+"</b>").css({"color":"#ce0c0c"}))
                      .append(fin[1]);
            return false;
        }
    }
}
var indic = 1;
function switchSit(obj){
    var text = getCookie("text");
    var motif = getCookie("motif");
    var l=$(obj).attr('data-sit');
    switch (l){
        case '0':
            if($(obj).attr('id')==='suiv'){
                if(assist_research(text,motif,indic,256,101)){
                    $("body > button").attr('data-sit',1);
                }
                else
                    indic++;
            }
            else{
                if(indic > 1){
                    assist_research(text,motif,indic-2,256,101);
                    indic--;
                }
                else{
                    document.location.href="../index.html"
                }   
            }
            break;
        case '1':
            if($(obj).attr('id')==='suiv'){
                if($(".sit0").attr("hidden")){
                   document.location.href="resume.html" 
                }
                else{
                    indic=1;
                    $(".sit0").attr("hidden","true");
                    $(".sit1").removeAttr("hidden");
                }
            }
            else{
                
                if($(".sit1").attr("hidden")){
                    indic--;
                    assist_research(text,motif,indic,256,101);
                    $("body > button").attr('data-sit',0);
                }else{
                    assist_research(text,motif,0,256,101);
                    $("body > button").attr('data-sit',0);
                    $(".sit1").attr("hidden","true");
                    $(".sit0").removeAttr("hidden");
                }
            }
            break;
        default :
            console.log("Fin des différentes situations");
    }
}
// faire 3 <p> pour séparer le texte et colorier la partie concerné