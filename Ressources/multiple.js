/*
 * Pas d'utilisation de hashtable pour cet algorithme de recherche de multiples motifs
 * Pas de hashtable possible en javascript de base
 * Des librairies existent sur internet mais sont difficiles à implémenter dans cet algorithme
*/
var nombreDeCaracteres = 256;
class Rabin_Karp_multiple{
    constructor(texte,mulMotifs,tailleMotif,nombrePremier,obj){
		
        this.texte = texte;
        
        this.mulMotifs = mulMotifs;
        this.tailleMotif = tailleMotif;
		
        this.nombrePremier=nombrePremier;
        
        this.hashChaine = new Array(this.mulMotifs.length).fill(0);
        
        this.hashTexte=0;
        this.hashValeur=1;
        
        this.index;
        this.indice;
        
        this.indexDeux;
        
        this.obj = obj;
    }
    recherche(){
        for(this.index=0; this.index < this.tailleMotif-1; this.index++){
			this.hashValeur = (this.hashValeur*nombreDeCaracteres)%this.nombrePremier;
        }
        //Calcul la valeur de hachage de la chaine et de la premiere portion du texte
        for(this.index = 0; this.index < this.tailleMotif; this.index++){
			for(this.indexDeux = 0; this.indexDeux < this.mulMotifs.length; this.indexDeux++)
				this.hashChaine[this.indexDeux] = (nombreDeCaracteres*this.hashChaine[this.indexDeux] + this.mulMotifs[this.indexDeux].charCodeAt(this.index))%this.nombrePremier;
            this.hashTexte = (nombreDeCaracteres*this.hashTexte + this.texte.charCodeAt(this.index))%this.nombrePremier;
        }
        //Fais glisser la chaine sur le texte 
        for(this.index=0; this.index <= this.texte.length - this.tailleMotif; this.index++){
            //Compare la valeur de hachage de chaque chaine et de la portion du texte actuelle
            //Si la valeur de hachage est égale alors on regarde ensuite les lettres une par une
            
            for(this.indexDeux = 0; this.indexDeux < this.mulMotifs.length; this.indexDeux++){
				
				if(this.hashChaine[this.indexDeux]===this.hashTexte){
					//On compare les caractères/lettres une par une
					for(this.indice = 0; this.indice < this.tailleMotif; this.indice++){
						if(this.texte.charCodeAt(this.indice+this.index)!==this.mulMotifs[this.indexDeux].charCodeAt(this.indice))
							break;
					}
					// if hashChaine == hashTexte et chaine.charCodeAt([0...this.chaine.length-1]) = this.texte.charCodeAt([index, index+1, ...index+this.chaine.length-1])
					if(this.indice===this.tailleMotif){
						this.obj.append($("<p class='result'>").text("Chaine " + this.mulMotifs[this.indexDeux] + " trouvé à l'indice "+(this.index+1)));
						//return ("Chaine trouvé à l'indice "+this.index);
					}
				}
				
            }
            
            //Calcul la valeur de hachage de la prochaine portion du texte en utilisant la propriété de l'empreinte de Rabin : retirer le bit de poids fort du hash courant et ajouter le bit de poids faible du hash suivant
            if(this.index < this.texte.length - this.tailleMotif){
                this.hashTexte = (nombreDeCaracteres*(this.hashTexte - this.texte.charCodeAt(this.index)*this.hashValeur)+this.texte.charCodeAt(this.index+this.tailleMotif))%this.nombrePremier;
                //au cas où le hash du texte est négative
                if(this.hashTexte < 0)
                    this.hashTexte = (this.hashTexte + this.nombrePremier);
            }
            
        }
        
    }

}

function init(){
    $(".result").remove();
    var div = $("#start");
    var textarea = $("#text");
    var input = $("input");
    if(textarea.val()!="" && input.val()!=""){
		
		var chaine = input.val();
		var mulMotifs = chaine.split(";");
		mulMotifs[0] = mulMotifs[0].trim();
		var tailleMotif = mulMotifs[0].length;
        for(var i = 1; i < mulMotifs.length; i++){
			mulMotifs[i] = mulMotifs[i].trim();
			if(tailleMotif != mulMotifs[i].length){
				alert("Les motifs n'ont pas la même taille!");
				return;
			}
		}
		
        var rk = new Rabin_Karp_multiple(textarea.val(),mulMotifs, tailleMotif,101,div);
        //div.append($("<p class='result'>").text("Texte : "+textarea.val()));
        //div.append($("<p class='result'>").text("Chaine : "+input.val()));
        rk.recherche();
        $(".result").hide();
        $(".result").show(800);
    }
    else{
        alert("Texte et/ou motif vide !");
    }
}
function remove(){
    $('.result').hide(400,function(){
        $(this).remove();
    });
}

//Voir pour afficher et colorier les endroits où la sous chaine apparait dans le texte
